#pragma once

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <iostream>

#include <math.h>
#define M_PI acosf(-1.0f)

#include <vector>
#include <array>

void drawSphere(float R = 5.0f, int sectors = 50, int strips = 50)
{
	float X0 = 0.0f, Y0 = 0.0f, Z0 = 1.0f;

	glColor3f(1.0f, 1.0f, 0.0f);
	glBegin(GL_TRIANGLE_STRIP);
	for (int i = 0; i < strips; i++)
	{
		float phi = 2.0f * M_PI * i / strips;
		float phi1 = 2.0f * M_PI * (i + 1) / strips;
		for (int j = 0; j <= sectors; j++)
		{
			float theta = 2.0f * M_PI * j / sectors;
			float theta1 = 2.0f * M_PI * (j + 1) / sectors;
			
			glVertex3f(X0 + R*cosf(phi)*cosf(theta), Y0 + R*sinf(phi), Z0 + R*cosf(phi)*sinf(theta));
			glVertex3f(X0 + R*cosf(phi1)*cosf(theta1), Y0 + R*sinf(phi1), Z0 + R*cosf(phi1)*sinf(theta1));
		}
	}
	glEnd();
}

