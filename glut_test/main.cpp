#include "global_funcs.h"

bool isWireframe = false;

float angle = 0.0f;

static float *vertices;
static float *colors;
static unsigned int *indices;

void drawSphere2(float R = 5.0f, int sectors = 5, int strips = 5)
{
	std::vector<std::array<float, 3>> colorsVec;
	std::vector<std::array<float, 3>> verticesVec;
}

//static float vertices[4 * 3] = 
//{
//	0.0f, 0.0f, 0.0f,
//	0.0f, 1.0f, 0.0f,
//	1.0f, 1.0f, 0.0f,
//	1.0f, 0.0f, 0.0f
//};
//static float colors[4 * 3] = 
//{
//	1.0f, 0.0f, 0.0f,
//	1.0f, 0.0f, 0.0f,
//	1.0f, 0.0f, 0.0f,
//	1.0f, 0.0f, 0.0f
//};
//
//static unsigned int indices[] =
//{
//	0,1,2,3,0
//};


void drawScene()
{
	glClear(GL_COLOR_BUFFER_BIT);

	if (isWireframe)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//glLoadIdentity();
	//glRotatef(angle, 0.0f, 1.0f, 0.0f);

	//glBegin(GL_TRIANGLES);
	//glColor3f(1.0f, 0.0f, 0.0f);
	//glVertex3f(1.0f, 1.0f, 2.0f);
	//glVertex3f(0.0f, 0.0f, 2.0f);
	//glVertex3f(2.0f, 0.0f, 2.0f);
	//glEnd();

	//glutWireSphere(3.0f, 20, 10);
	//drawSphere2();

	//glDrawElements(GL_TRIANGLE_STRIP, 5, GL_UNSIGNED_INT, indices);
	glBegin(GL_TRIANGLE_STRIP);
	for (int i = 0; i < 12; i++)
		glArrayElement(i % 12);
	glArrayElement(2);
	glArrayElement(3);
	glArrayElement(0);
	glEnd();

	glFlush();
}

void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-10.0, 10.0, -10.0, 10.0, -1.0, 10.0);
	//glFrustum(-1.0, 1.0, -1.0, 1.0, 1., 20.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'w':
		std::cout << "wireframe mode\n";
		isWireframe = true;
		break;
	case 's':
		std::cout << "surface mode\n";
		isWireframe = false;
		break;
	default:
		break;
	}

	glutPostRedisplay();
}

void setup()
{
	glClearColor(0.3f, 0.4f, 0.4f, 0.0f);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, vertices);
	glColorPointer(3, GL_FLOAT, 0, colors);
}

void specialKeyInput(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP:
		std::cout << angle << "\n";
		angle += 1.0f;
		break;
	case GLUT_KEY_DOWN:
		std::cout << angle << "\n";
		angle -= 1.0f;
	default:
		break;
	}
	glutPostRedisplay();
}

int main(int argc, char ** argv)
{
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("test");

	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);
	glutSpecialFunc(specialKeyInput);

	glewExperimental = GL_TRUE;
	glewInit();

	setup();

	glutMainLoop();

	return 0;
}
